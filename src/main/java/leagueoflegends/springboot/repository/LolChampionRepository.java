package leagueoflegends.springboot.repository;

import leagueoflegends.springboot.entities.LolChampion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository
public interface LolChampionRepository extends JpaRepository <LolChampion, Long> {
}
