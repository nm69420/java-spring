package leagueoflegends.springboot.controller;


import leagueoflegends.springboot.entities.LolChampion;
import leagueoflegends.springboot.service.LolChampionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/api/v1/lolchampion")

public class LolChampionController {

    @Autowired
    private LolChampionService lolChampionService;

    @GetMapping
    public List<LolChampion> findAll() {
        return lolChampionService.findall();
    }

    @GetMapping("/{id}")
    public ResponseEntity<LolChampion> findById(@PathVariable long id){
        try {
            return new ResponseEntity<LolChampion>(lolChampionService.findById(id), HttpStatus.CREATED);
        } catch (NoSuchElementException ex) {
            //return 404
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<LolChampion> create(@RequestBody LolChampion lolChampion) {
        return new ResponseEntity<LolChampion>(lolChampionService.save(lolChampion), HttpStatus.CREATED);
    }

}
