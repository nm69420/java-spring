package leagueoflegends.springboot.service;

import leagueoflegends.springboot.entities.LolChampion;
import leagueoflegends.springboot.repository.LolChampionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class LolChampionService {

    @Autowired
    LolChampionRepository lolChampionRepository;

    public List<LolChampion> findall() {
        return lolChampionRepository.findAll();
    }

    public LolChampion findById(long id) {
        return lolChampionRepository.findById(id).get();
    }

    public LolChampion save(LolChampion lolChampion) {
        return lolChampionRepository.save(lolChampion);
    }
}
